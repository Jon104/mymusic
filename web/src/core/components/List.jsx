import React from "react";
import PropTypes from "prop-types";
import ListEntry from "./ListEntry";

export default class List extends React.PureComponent {
  static propTypes = {
    elements: PropTypes.objectOf(PropTypes.shape({})),
    handleDelete: PropTypes.func.isRequired,
    setSelectedSong: PropTypes.func.isRequired
  };

  static defaultProps = {
    elements: []
  };

  render() {
    return (
      <>
        <ul>
          {Object.values(this.props.elements).map(element => (
            <ListEntry
              key={element._id}
              file={element}
              deleteMusic={this.props.handleDelete}
              setSelectedSong={this.props.setSelectedSong}
            />
          ))}
        </ul>
      </>
    );
  }
}
