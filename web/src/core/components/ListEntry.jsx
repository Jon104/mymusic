import React from "react";
import PropTypes from "prop-types";
import { removeExtension } from "../../PublicContainer";
import Typography from "@material-ui/core/Typography";

export default class ListEntry extends React.PureComponent {
  static propTypes = {
    file: PropTypes.object.isRequired,
    deleteMusic: PropTypes.func.isRequired,
    setSelectedSong: PropTypes.func.isRequired
  };

  handleDelete = () => {
    this.props.deleteMusic(this.props.file._id);
  };

  handleSetSelectedSong = () => {
    this.props.setSelectedSong(
      this.props.file._id.concat(this.props.file.name)
    );
  };

  render() {
    return (
      <>
        <li id="list_entry" onClick={this.handleSetSelectedSong}>
          <Typography variant="body2" className="entry-name">
            {removeExtension(this.props.file.name)}
          </Typography>
          <i
            className="button material-icons pointer to_the_right "
            onClick={this.handleDelete}
          >
            delete
          </i>
        </li>
      </>
    );
  }
}
