import { START_FETCHING, STOP_FETCHING } from './types';

export const startFetching = entityName => ({ type: START_FETCHING, payload:entityName });
export const stopFetching = entityName => ({ type: STOP_FETCHING, payload:entityName });