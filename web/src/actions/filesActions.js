import axios from 'axios';
import { DELETE_FILE, GET_FILES, SET_FILE } from './types';
import { startFetching, stopFetching } from './appActions';

function postMusic(userId, data, token) {
  return axios.post(`/users/${userId}/music`, data);
}

function getMusic(userId, token) {
  return axios.get(`/music/${userId}`);
}

function deleteMusicCall(userId, fileId, token) {
  return axios.delete(`/users/${userId}/music/${fileId}`);
}

export const uploadFile = file => async dispatch => {
  startFetching('uploaded-file');
  try {
    const formData = new window.FormData();
    const userId = window.localStorage.getItem("userId-06");
    const token = window.localStorage.getItem("token-06");

    formData.append('formfile', file);
    const response = await postMusic(userId, formData, token);

    const fileMetadata = response.data;
    
    dispatch({ type: SET_FILE, payload: fileMetadata });
  } catch (error) {
    dispatch(error);
  }
  stopFetching('uploaded-file');
}

export const fetchMusic = () => async dispatch => {
  startFetching('get-file');
  try {
    const userId = window.localStorage.getItem("userId-06");
    const token = window.localStorage.getItem("token-06");
    const response = await getMusic(userId, token);
    dispatch({ type: GET_FILES, payload: response.data });
  } catch (error) {
    dispatch(error);
  }
  stopFetching('get-file');
}

export const deleteMusic = (fileId) => async dispatch => {
  startFetching('delete-file');
  try {
    const userId = window.localStorage.getItem("userId-06");
    const response = await deleteMusicCall(userId, fileId);
    dispatch({ type: DELETE_FILE, payload: response.data });
  } catch (error) {
    dispatch(error);
  }
}