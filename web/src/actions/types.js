export const DELETE_FILE = 'DELETE_FILE';
export const GET_FILES = 'GET_FILES';
export const SET_FILE = 'SET_FILE';
export const SET_FILES = 'SET_FILES';
export const START_FETCHING = 'START_FETCHING'
export const STOP_FETCHING = 'STOP_FETCHING'
export const SET_FILES_REQUESTED = "SET_FILES_REQUESTED";
export const SET_FILES_RECEIVED = "SET_FILES_RECEIVED";
export const SET_FILES_FAILED  = "SET_FILES_FAILED";