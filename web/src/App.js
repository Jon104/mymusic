import React from "react";
import "./App.css";
import Home from "./components/Home";
import Login from "./components/Login";
import LogoutButton from "./components/LogoutButton";
import {
  BrowserRouter,
  HashRouter,
  Redirect,
  Switch,
  Route
} from "react-router-dom";
import { setDefaultsFromLocalStorage } from "./axios";
import NavBar from "./components/NavBar";

function getParameterByName(name) {
  const url = window.location.href;
  name = name.replace(/[[]]/g, "\\$&");
  const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
  const results = regex.exec(url);
  if (!results) {
    return null;
  }
  if (!results[2]) {
    return "";
  }
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

const PrivateRoute = ({ component: Component, ...rest }) => {
  const newAccessToken = getParameterByName("accessToken");
  const newUserId = getParameterByName("userId");
  const currentToken = window.localStorage.getItem("token-06");
  if (
    newAccessToken &&
    newAccessToken !== "" &&
    newAccessToken !== currentToken
  ) {
    window.localStorage.setItem("token-06", newAccessToken);
    if (newUserId && newUserId !== "") {
      window.localStorage.setItem("userId-06", newUserId);
    }
    setDefaultsFromLocalStorage();
    document.location.href = "/";
  }
  return (
    <Route
      {...rest}
      render={props =>
        currentToken && window.localStorage.getItem("userId-06") ? (
          <Component {...props} />
        ) : (
          <Redirect to="/login" />
        )
      }
    />
  );
};

function App() {
  const isUserLoggedIn = window.localStorage.getItem("userId-06") !== null;
  return (
    <div className="app-container">
      {isUserLoggedIn && <NavBar />}
      <LogoutButton />
      <BrowserRouter>
        <HashRouter>
          <Switch>
            <PrivateRoute
              exact={true}
              path="/"
              title={"Home"}
              component={Home}
            />
            <Route path="/login" title={"Login"} render={() => <Login />} />
          </Switch>
        </HashRouter>
      </BrowserRouter>
    </div>
  );
}

export default App;
