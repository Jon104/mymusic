import * as constants from "./constants";

export const removeExtension = fileName =>
  isEmpty(fileName) ? fileName : fileName.replace(/\.[^/.]+$/, "");

export function throwError(customMessage, error) {
  return {
    customMessage,
    error,
    type: constants.THROW_ERROR
  };
}

export const isEmpty = obj =>
  [Object, Array].includes((obj || {}).constructor) &&
  !Object.entries(obj || {}).length;
