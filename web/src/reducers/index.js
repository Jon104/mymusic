import { combineReducers } from 'redux';

import filesReducer from './filesReducer';

const rootReducer = combineReducers({
  filesById: filesReducer,
});

export default rootReducer;