import { GET_FILES, SET_FILE, SET_FILES, DELETE_FILE } from '../actions/types';
import _keyBy from 'lodash.keyby';

const initialState = {
  filesById: {},
};

const filesReducer = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_FILE:
      document.location.reload(true);
      return { ...state };
    case GET_FILES:
      return { ...state, filesById: _keyBy(action.payload.items, '_id') };
    case SET_FILE:
      return { ...state, filesById: { ...state.filesById, [action.payload.id]: action.payload } };
    case SET_FILES:
      return { ...state, filesById: { ...state.filesById, ...action.filesById.data.items } };
    default:
      return state;
  }
}

export default filesReducer;