import React from "react";
import PropTypes from "prop-types";
import List from "../core/components/List";
import { connect } from "react-redux";
import { deleteMusic, fetchMusic } from "../actions";
import MusicPlayer from "./MusicPlayer";

class LibraryPage extends React.Component {
  static propTypes = {
    files: PropTypes.objectOf(PropTypes.shape({})), //TODO proper type
    fetchMusic: PropTypes.func.isRequired
  };

  state = {
    selectedSong: ""
  };

  static defaultProps = {
    files: []
  };

  componentWillMount() {
    this.props.fetchMusic();
  }

  handleDeleteMusic = fileId => {
    this.props.deleteMusic(fileId);
  };

  setSelectedSong = selectedSong => {
    this.setState({ selectedSong });
  };

  render() {
    return (
      <>
        <section id="music_table">
          <List
            elements={this.props.files}
            handleDelete={this.handleDeleteMusic}
            setSelectedSong={this.setSelectedSong}
          />
        </section>
        <MusicPlayer songToPlay={this.state.selectedSong} />
      </>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { filesById } = state.filesById;

  return {
    files: filesById
  };
};

export default connect(
  mapStateToProps,
  {
    deleteMusic,
    fetchMusic
  }
)(LibraryPage);
