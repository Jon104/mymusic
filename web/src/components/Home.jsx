import React from 'react';
import LibraryPage from './LibraryPage';

export default class Home extends React.Component {
  render() {
    return (
      <>
        <LibraryPage />
      </>
    );
  }
}