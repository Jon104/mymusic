import React from "react";
import AddBox from "@material-ui/icons/AddBox";
import { isEmpty } from "../PublicContainer";
import { connect } from "react-redux";
import { fetchMusic, uploadFile } from "../actions";

class NavBar extends React.PureComponent {
  inputRef = React.createRef();
  inputId = 234;

  handleChange = async e => {
    if (!isEmpty(e.currentTarget.files)) {
      const file = e.currentTarget.files[0];
      // TODO add verification of allowed extension (.env of projects)
      await this.props.uploadFile(file);
      this.props.fetchMusic();
    }
    e.currentTarget.value = "";
  };

  render() {
    return (
      <aside className="navbar">
        <div className="mx-wdt-150">
          <img
            className="mx-wdt-150
            "
            src={require("../assets/MyMusicLogo.jpg")}
            alt="MyMusic Logo"
            max-width="250"
          />
        </div>

        <div>
          <input
            id={this.inputId}
            ref={this.inputRef}
            name={"importFile"}
            //value={}
            type="file"
            onChange={this.handleChange}
            className="hide"
          />

          <label className="button" htmlFor={this.inputId}>
            <AddBox fontSize="large" />
          </label>
        </div>
      </aside>
    );
  }
}

const mapStateToProps = props => {
  return {
    ...props
  };
};

export default connect(
  mapStateToProps,
  {
    fetchMusic,
    uploadFile
  }
)(NavBar);
