import React from "react";
import Button from "@material-ui/core/Button";
import { REACT_APP_API_URL } from "../constants";
import Typography from "@material-ui/core/Typography";

export default class Login extends React.PureComponent {
  handleLogin = () => {
    window.location.assign(`${REACT_APP_API_URL}/auth/google`);
  };

  render() {
    return (
      <>
        <div className="panels home_left">
          <div>
            <img
              className="inline-block"
              src={require("../assets/MyMusicLogo.jpg")}
              alt="MyMusic logo"
              height="42"
              width="42"
            />
          </div>
          <div className="container">
            <Typography className="inline-block" color="inherit" variant="h3">
              MyMusic
            </Typography>
            <h6>Howdie you from the futur.</h6>
            <h6>
              Welcome to MyMusic, the music player that lets you store your
              music. Hence becoming your music library, your MyMusic... and not
              MyMusic to me as if i would be sharing my own music. This is your
              own MyMusic of your music.. .your MyMusic... Ahh bock off.
            </h6>
          </div>
        </div>
        <div className="panels home_right container_table">
          <div className="vertical_align_table_child">
            <div className="container">
              <Button
                variant="outlined"
                className="button"
                onClick={this.handleLogin}
              >
                Continue with Google
              </Button>
            </div>
          </div>
        </div>
      </>
    );
  }
}
