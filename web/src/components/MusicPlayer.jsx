import React from "react";
import PropTypes from "prop-types";

const cloudFrontUrl = "https://d2274ohjlmmhuz.cloudfront.net/";

export default class MusicPlayer extends React.Component {
  static propTypes = {
    songToPlay: PropTypes.string
  };

  static defaultProps = {
    songToPlay: ""
  };

  render() {
    const songUrl = cloudFrontUrl.concat(this.props.songToPlay);
    return (
      <>
        <div id="music_player" className="align-childs-verticaly bottom">
          <audio controls src={songUrl} />
        </div>
      </>
    );
  }
}
