import React from "react";
import { logout } from "../services/authServices";

export default class LogoutButton extends React.Component {
  onLogout = () => {
    logout();
  };

  render() {
    return (
      <>
        <div className="top-right">
          <label className="button ft-sz-sm bold" onClick={this.onLogout}>
            Logout
          </label>
        </div>
      </>
    );
  }
}
