export const logout = () => {
  window.localStorage.clear();
  document.location.href = "/#/login";
  document.location.reload();
};
