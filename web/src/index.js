import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from './reducers';
import thunk from 'redux-thunk';

import './index.css';
import App from './App';

import * as serviceWorker from './serviceWorker';

const middleware = [require('redux-immutable-state-invariant').default(), thunk];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store =
  createStore(
    rootReducer,
    composeEnhancers(
      applyMiddleware(...middleware)
    )
  );

const rootElement = document.getElementById('root');
ReactDOM.render(
  <Provider store={store}>
    <App />,
  </Provider>,
  rootElement
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
