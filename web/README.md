[![Netlify Status](https://api.netlify.com/api/v1/badges/5b2f84ac-eec4-4b72-890c-838a9f1e8722/deploy-status)](https://app.netlify.com/sites/sad-joliot-db702b/deploys)

## Available Scripts

### `yarn`

This will install dependencies from package.js

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## To launch backend

Clone the repository: git clone https://Jon104@bitbucket.org/Jon104/my-music-backend.git
Add .env file.

Run yarn && yarn start and voilà.
